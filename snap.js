function snapCrackle(maxValue) {

    let frase = " ";

    for (let i = 1; i <= maxValue; i++) {

        if (i % 5 === 0 && !(i % 2 === 0)) {

            frase += "SnapCrackle, ";

        } else if (i % 2 !== 0) {

            frase += "Snap, ";

        } else if (i % 5 === 0) {

            frase += "Crackle, ";

        } else {

            frase += i + ", ";

        }

    }
    console.log(frase);

    return frase

}
snapCrackle(12);

// ***** BONUS *****

function numeroPrimo(num) {

    let divide = 0;

    for (let i = 1; i <= num; i++) {

        if (num % i === 0) {
            divide++
        }
    }

    if (divide === 2) {

        return true;

    } else {

        return false;
    }
}

function snapCracklePrime(maxValue) {

    let frase = [];
    let snap = "snap"
    let prime = "Prime"
    let crackle = "Crackle"

    for (let i = 1; i <= maxValue; i++) {

        if (numeroPrimo(i) && i === 2) {

            frase.push(prime);

        } else if (numeroPrimo(i) && i % 5 === 0 && i % 2 !==0) {

            frase.push(snap + crackle + prime);

        }  else if (!numeroPrimo(i) && i % 5 === 0 && i % 2 !==0) {

            frase.push(snap + crackle);

        } else if (numeroPrimo(i) % 2 !==0) {

            frase.push(snap + prime);

        } else if (!numeroPrimo(i) && i % 2 !==0) {

            frase.push(snap);
        } else {

            frase.push(i);
        }
    }
    let fraseSnap = frase.join (", ") ;
    return fraseSnap
}
        snapCracklePrime();

